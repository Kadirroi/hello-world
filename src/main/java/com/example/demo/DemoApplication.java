package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "home";
	}
	
	@GetMapping("/health")
	public String imHealthy() {
		return "{healthy:true}";
	}
	
	@GetMapping("/ready")
	public String imReady() {
		return "{ready:true}";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}